'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Product, { foreignKey: "UserId" });
    }
  };
  User.init({
    username: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `username tidak boleh kosong`,
        },
      },
    },
    email: {
      type: DataTypes.STRING,
      validate: {
         notEmpty: {
          args: true,
          msg: `email tidak boleh kosong`,
        },
        isEmail: {
          args: true,
          msg: `silahkan diisi format email!`,
        },
      },
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `pass tidak boleh kosong`,
        },
        // len: {
        //   args: [6, 10],
        //   msg: `panjang karaketer password antara 6-10`,
        // },
      },
    },
    role: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `masukkan role anda!`,
        },
      },
    }
  }, 
  {
    sequelize,
    modelName: 'User',
  });
  return User;
};