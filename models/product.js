'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Product.belongsTo(models.User);
    }
  };
  Product.init({
    description: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `silahkan diisi desckirpsi produk`,
        },
      },
    },
    specification: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `silahkan diisi spec produk`,
        },
      },
    },
    color: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `silahkan dipilih warna produk`,
        },
      },
    },
    size: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: `silahkan diisi ukuran produk`,
        },
      },
    },
  }, 
  {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};