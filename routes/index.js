const express = require('express')
const router = express.Router()
const ProductRoute = require('./ProductRoute')
const AdminRoute = require('./AdminRoute');
const VendorRoute = require('./VendorRoute');


router.use('/product', ProductRoute);
router.use('/admin', AdminRoute)
router.use('/vendor', VendorRoute)

module.exports = router;