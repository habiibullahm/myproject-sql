const express = require('express')
const router = express.Router()

const AdminController = require('../controller/AdminController');
const { authenticate, authorize} = require("../middlewares/auth");

router.post('/register', AdminController.postRegister);
router.post('/login', AdminController.postLogin );
router.get('/',authenticate, AdminController.getAdmin)
router.put('/:id',authenticate, AdminController.putAdmin)
router.delete('/:id',authenticate, AdminController.deleteAdmin)

module.exports = router;