const express = require('express')
const router = express.Router()

const VendorController = require('../controller/VendorController');
const { authenticate, authorize} = require("../middlewares/auth");

router.post('/register',authenticate, VendorController.postRegister);
router.post('/login',VendorController.postLogin );
router.get('/',authenticate, VendorController.getVendor)
router.put('/:id',authenticate, VendorController.putVendor)
router.delete('/:id',authenticate, VendorController.deleteVendor)

module.exports = router;