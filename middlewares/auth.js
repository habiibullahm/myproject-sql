const { Product, User } = require("../models");
const { verifyToken } = require("../helpers/jwt");

function authenticate(req, res, next) {
  const { access_token } = req.headers;
  console.log(`Ini dari headers`, req.headers);
  if (access_token) {
    const decoded = verifyToken(access_token);
    console.log(`data decode`, decoded);
    User.findOne({
      where: {
        email: decoded.email
      },
    })
      .then((data) => {
        console.log('data login', data);
        if (!data) {
          res.status(401).json({ msg: `Invalid access token!` });
        } else {
          if(data.role !== 'admin') {
           return res.status(401).json({ msg : 'anda bukan admin !'})
          }
          res.user= { UserId: data.id, email: data.email, role: data.role };
          next();
        }
      })

      .catch((err) => {
        console.log(err);
        res.status(500).json({msg: `Internal server is an error!`,err});
      });
  } else {
    res.status(401).json({ msg: `Invalid access token!` });
  }
}

function authorize(req, res, next) {
  const id = +req.params.id;

  User.findOne({ where: { id } })
    .then((data) => {
      if (data) {
        const valid = req.User.id === Product.UserId;
        if (valid) {
          console.log('verified')
          next();
        } else {
          res.status(401).json(`Unauthorized`);
        }
      } else {
        res.status(401).json(`Unauthorized`);
      }
    })

    .catch((err) => {
      console.log(err);
      res.status(500).json({ msg: `Internal server is an error!` });
    });
}

function authorizeProduct(req, res, next) {
  const id = +req.params.id;

  User.findOne({ where: { id } })
    .then((data) => {
      if (data) {
        const valid = req.User.id === Product.UserId;
        if (valid) {
          console.log('verified')
          next();
        } else {
          res.status(401).json(`Unauthorized`);
        }
      } else {
        res.status(401).json(`Unauthorized`);
      }
    })

    .catch((err) => {
      console.log(err);
      res.status(500).json({ msg: `Internal server is an error!` });
    });
}

module.exports = {
  authenticate,
  authorize,
};
