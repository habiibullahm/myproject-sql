const { Product } = require("../models");

class ProductController {
  static async postProduct(req, res) {
    try {
      const {description, specification, color, size } = req.body;
      const UserId = res.user.UserId
      console.log(UserId);
      console.log('res abcdh', res);
      const data = await Product.create({
        description,
        specification,
        color,  
        size,
        UserId : UserId  
      });
      res.status(201).json({ msg: `produk successfully created`, data });
    } catch (err) {
      res.status(500).json({ msg: `internal server is an error!`, err });
    }
  }

  static async getProduct(req, res) {
    try {
      let data = await Product.findAll();

      if (!data.length) return res.status(500).json({ msg: `Datanya semua produk kosong!` });

      res.status(200).json({ msg: 'Produknya ada', data });
    } catch (err) {
      console.log(err);
      res.status(500).json({ msg: `internal server is an error!` });
    }
  }

  static async getOneProduct(req, res) {
    try {
      const { id } = req.params;
      let data = await Product.findOne({
        where: {
          id: id,
        }
      });

      if (!data) return res.status(500).json({ msg: `Datanya kosong !` });

      res.status(200).json({ msg: data });
    } catch (err) {
      console.log(err);
      res.status(500).json({ msg: `internal server is an error!` });
    }
  }

  static putProduct(req, res) {
    const id = req.params.id;
    const { description, specification, color, size } = req.body;

    Product.update(
      {
        description, 
        specification, 
        color, 
        size
      },
      { where: { id }, returning: true }
    )

      .then((data) => {
        if(data[1] === 1) {
          res
          .status(200)
          .json({ msg: `Successfully edited task id: ${id}`, data});
        } else {
          res.status(500).json({ msg: `produk tidak ditemukan` });
        }
        
      })

      .catch((err) => {
        console.log(err);
        res.status(500).json({ msg: `internal server is an error!` });
      });
  }

  static deleteProduct(req, res) {
    const id = +req.params.id;

    Product.destroy({
      where: { id },
    })

      .then((data) => {
        if (!data) {
          res.status(404).json({ msg: `Product is not found` });
        } else {
          res.status(200).json({ msg: `Product has been deleted id: ${id}` });
        }
      })

      .catch((err) => {
        console.log(err);
        res.status(500).json({ msg: `internal server is an error!` });
      });
  }
}

module.exports = ProductController;
