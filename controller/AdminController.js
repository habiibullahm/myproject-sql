const { User } = require("../models");

const { comparePassword, hashPassword } = require("../helpers/bcrypt");
const { generateToken } = require("../helpers/jwt");
// const { hash } = require("bcrypt");

class AdminController {
  static postRegister(req, res) {
    const { username, email, password} = req.body;
    User.create({
      username,
      email,
      password : hashPassword(password),
      role:'admin'
    })

      .then((data) => {
        let admin = {
          id: data.id,
          username: data.username,
          email: data.email,
          role: data.role
        };

        res.status(201).json({ msg: `Admin berhasil dibuat!`, admin });
      })

      .catch((err) => {
        res.status(500).json({ msg: err.errors[0].message || `Internal server is an error!`});
      });
}

  static postLogin(req, res) {
    const { email, password } = req.body;
    User.findOne({
      where: {
        email: email,
      },
    })

      .then((data) => {
        console.log('data ketika login', data);
        if (data) {
        
          let checkPassword = comparePassword(password, data.password);

          if (!checkPassword) {
            return res.status(401).json({ msg: `Invalid email/password` });
          }
          if (data.role !== 'admin') {
            return res.status(401).json({ msg : 'you are not admin'})
          }

          let payload = {
            id: data.id,
            email: data.email,
            role : data.role
          };

          const access_token = generateToken(payload);
          res.status(200).json({ access_token });
        } else {
          res.status(401).json({ msg: `Invalid email/password` });
        }
      })

      .catch((err) => {
        console.log(err);
        res.status(500).json({ msg: `Internal server is an error!` });
      });
}

  static async getAdmin(req, res) {
    try {
      let data = await User.findAll({
        where : {
          role : 'admin',
        },
        attributes: {exclude: ['password']}
      })
      
      if(!data.length) return res.status(500).json({
        msg: 'tidak ada data admin'
      });
      res.status(200).json({ msg: 'data admin', data})
  } catch (err) {
    console.log(err);
    res.status(500).json({msg:'internal server is an error'});
  }
}

  static async putAdmin(req, res) {
    try {
      const id = req.params.id;
      const {username, email, password} = req.body

      const data = await User.update({
        username,
        email,
        password:hashPassword(password)
      },
      { where : {id}, returning : true }
      )
      res.status(201).json({ msg : 'successfully updated data admin'})
    } catch (err) {
      console.log(err);
      res.status(500).json({msg : 'internal server is an error', err});
    }
  } 

  static async deleteAdmin(req, res) {
    try {
      const id = req.params.id
      const data = await User.destroy({
        where : { id },
      })
      res.status(201).json({ msg: `admin byid ${id} has beed deleted`})
    } catch (err){
      console.log(err);
      res.status(500).json({ msg: "internal server is an error"});
    }
  }
}
module.exports = AdminController;
